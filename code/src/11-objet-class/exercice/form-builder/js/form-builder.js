// eslint-disable-next-line no-unused-vars
const FormBuilder = (function () {
    'use strict'

    class FieldBuilder {
        constructor (field) {
            this.field = field
        }

        createElement (tagName, attributes) {
            const element = document.createElement(tagName)

            if (attributes) {
                attributes.forEach(function (attribute) {
                    element.setAttribute(attribute.name, attribute.value)
                })
            }

            return element
        }

        build () {
            const divElement = this.createElement('div')
            const labelElement = this.createElement('label', [{ name: 'for', value: this.field.id }])
            labelElement.appendChild(document.createTextNode(this.field.label))
            labelElement.classList.add(`label-${this.field.type}`)

            divElement.appendChild(labelElement)

            return divElement
        }

        createFieldElement (tagName, attributes) {
            const fieldElement = this.createElement(tagName, attributes)
            fieldElement.setAttribute('id', this.field.id)
            fieldElement.setAttribute('name', this.field.id)

            return fieldElement
        }
    }

    class InputFieldBuilder extends FieldBuilder {
        build () {
            const divElement = super.build()
            const inputElement = this.createFieldElement('input')
            inputElement.setAttribute('type', this.field.type)

            if (this.field.required) {
                inputElement.setAttribute('required', this.field.required)
            }

            divElement.appendChild(inputElement)

            return divElement
        }
    }

    class TextAreaFieldBuilder extends FieldBuilder {
        build () {
            const divElement = super.build()
            const textAreaElement = this.createFieldElement('textarea')

            if (this.field.required) {
                textAreaElement.setAttribute('required', this.field.required)
            }

            divElement.appendChild(textAreaElement)

            return divElement
        }
    }

    function buildField (field) {
        let fieldElement

        switch (field.type.toLowerCase()) {
        case 'text':
        case 'password':
        case 'number':
        case 'date':
            fieldElement = new InputFieldBuilder(field)
            break
        case 'textarea':
            fieldElement = new TextAreaFieldBuilder(field)
            break
        default:
            break
        }

        return fieldElement.build()
    }

    function createButton () {
        const button = document.createElement('button')
        button.setAttribute('type', 'submit')
        button.appendChild(document.createTextNode('Update'))

        return button
    }

    return {
        init: function (form, result) {
            const formElement = document.getElementById(form.id)

            form.fields.forEach(function (field) {
                formElement.appendChild(buildField(field))
            })

            formElement.appendChild(createButton())

            formElement.addEventListener('submit', function (event) {
                event.preventDefault()

                const resultObject = {}
                Object.values(formElement).forEach(function (domElement) {
                    if (domElement.name) {
                        resultObject[domElement.name] = domElement.value
                    }
                })

                result.innerHTML = JSON.stringify(resultObject, null, '    ')
            })
        }
    }
})()
