'use strict'

// eslint-disable-next-line no-unused-vars
const Collapsible = (function () {
    const COLLAPSIBLE_H = 'collapsible-h'
    const COLLAPSIBLE_DIV = 'collapsible-div'
    const COLLAPSIBLE_OPEN = 'collapsible-open'
    const COLLAPSIBLE_CLOSE = 'collapsible-close'

    function onClickEventHandler (hTag, divTag) {
        if (divTag.style.display === 'block') {
            divTag.style.display = 'none'
            hTag.classList.remove(COLLAPSIBLE_OPEN)
            hTag.classList.add(COLLAPSIBLE_CLOSE)
        } else {
            divTag.style.display = 'block'
            hTag.classList.remove(COLLAPSIBLE_CLOSE)
            hTag.classList.add(COLLAPSIBLE_OPEN)
        }
    }

    function privateCollapsible (element) {
        const hElement = element.querySelector('h2')
        const divElement = element.querySelector('div')

        hElement.classList.add(COLLAPSIBLE_H, COLLAPSIBLE_CLOSE)
        hElement.addEventListener('click', function () {
            onClickEventHandler(hElement, divElement)
        })

        divElement.classList.add(COLLAPSIBLE_DIV)
        divElement.style.display = 'none'
    }

    return {
        init: function (config) {
            const elements = Array.from(document.getElementsByClassName(config.className))

            elements.forEach(privateCollapsible)
        }
    }
})()
