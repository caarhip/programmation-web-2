'use strict'

/**
 * Compléter le code manquant pour obtenir le même résultat que les maquettes
 *
 * Le principe est le suivant:
 * - L'onglet sélectionné reçoit la classe courant
 * - Le div qui correspond au lien (htag) de cet onglet est montré, les autres cachés
 */
