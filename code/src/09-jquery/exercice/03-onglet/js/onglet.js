'use strict'

/**
 * Compléter le code manquant pour obtenir le même résultat que les maquettes
 *
 * Le principe est le suivant:
 * - L'onglet sélectionné reçoit la classe courant
 * - Le div qui correspond au lien (htag) de cet onglet est montré, les autres cachés
 */
$(document).ready(function () {
    $('#info-nav li:first-of-type').addClass('courant')
    $('#tabs-2').hide()
    $('#tabs-3').hide()

    let activeTab = $('#info-nav li a:first-of-type').attr('href')

    $('#info-nav li a').click(function (event) {
        event.preventDefault()

        activeTab = $('#info-nav li.courant a').attr('href')

        $('#info-nav li').removeClass('courant')

        $(this).parents('li').addClass('courant')

        $(activeTab).hide()
        const targetTab = $(this).attr('href')
        $(targetTab).show()
    })
})
