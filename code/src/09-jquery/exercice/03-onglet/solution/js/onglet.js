'use strict'

/**
 * Le principe est le suivant:
 * - L'onglet sélectionné reçoit la classe courant
 * - Le div qui correspond au lien (htag) de cet onglet est montré, les autres cachés
 */

$(function () {
    console.log('jQuery chargé - DOM construit')
    // sélectionner et cacher tous les divs #info (sauf le premier)
    $('#info div:not(:first)').hide()

    // Sélectionner les li de navigation et écouter sur le click
    $('#info-nav li').click(function (event) {
        // Évite le scroll automatique du navigateur
        event.preventDefault()

        // Cacher tous les div de #info
        $('#info div').hide()

        // Enlever la classe .courant à l'onglet qui a la classe courant
        $('#info-nav .courant').removeClass('courant')
        // Mettre la classe courant au nouvel onglet courant
        $(this).addClass('courant')

        // Sélectionner le premier élément <a> parmi les enfants du <li> et récupérer son attribut href=""
        var clickedTab = $(this).find('a:first').attr('href')

        // Faire apparaitre le bon div dans #info
        $('#info ' + clickedTab).fadeIn('fast')
    }).eq(0).addClass('courant')
})
