'use strict'

const DIM = {
    min: 1,
    max: 15
}

$(function () {
    const newTable =
        $('<table id="multiplication"></table>')
            .appendTo('#wrapper')

    // Créer une ligne d'en-tête avant les lignes de <td>
    const newTrHeader = $('<tr></tr>')
        .appendTo(newTable)
        .append('<th></th>')

    // Finir de remplir le 1ere ligne avec des th qui ont un contenu
    for (let j = DIM.min; j <= DIM.max; j++) {
        $('<th></th>')
            .appendTo(newTrHeader)
            .html(j)
    }

    // Créer les lignes td suivantes
    for (let i = DIM.min; i <= DIM.max; i++) {
        const newTrBody = $('<tr></tr>')
            .appendTo(newTable)
            .append('<th>' + i + '</th>')

        // Créer les td dans le tr
        for (let j = DIM.min; j <= DIM.max; j++) {
            $('<td></td>')
                .appendTo(newTrBody)
                .html(i * j)
        }
    }
})
