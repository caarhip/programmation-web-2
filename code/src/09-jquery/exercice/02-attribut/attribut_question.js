'use strict'

/**
 * Compléter le code manquant pour obtenir le même résultat que la maquette ci-dessous et le même résultat dnas la console
 */

console.log('Salut jQuery !')

// todo (1) : Sélectionnez tous les p du document et mettez cette sélection dans une variable nommée pElements
console.groupCollapsed('1')
let pElements = $('p')
console.log('todo (1): pElements:', ????)
console.groupEnd()

// todo (2) : Partez de la variable pElements et affichez dans la console le contenu texte du PREMIER de ces paragraphes (méthodes .eq() et .text())
console.groupCollapsed('2')
console.log('todo (2):', ????)
console.groupEnd()

// todo (3) : Affichez son contenu HTML (méthode .html())
console.groupCollapsed('3')
console.log('todo (3):', ????)
console.groupEnd()

// todo (4) : Changer le contenu HTML (méthode .html()) de tous les titres h2 de la page en "Contenus avec <strong>jQuery</strong>"
console.groupCollapsed('4')
console.log('todo (4):', ????)
console.groupEnd()

// todo (5) : Sélectionnez les éléments de classe "costaud" et placez les dans une variable nommée costauds
console.groupCollapsed('5')
let costaudElements = $('.costaud')
console.log('todo (5):costauds:', ????)

console.groupEnd()

// todo (6) : Sélectionnez tous les li du menu (le nav du header) et placez les dans une variable nommée menuLiElements.
// Loguez la variable. À partir de menuLiElements,  sélectionnez l'hyperlien enfant du SECOND li ( .eq(), .children() ),
// placez le dans une variable nommée linkElements. Logguez le dans la console.
// Indiquez dans la console si cet hyperlien a la classe "active" ou pas (que ce soit explicite dans la console s'il a la classe ou pas).
// Utilisez la méthode .hasClass(). Ecrivez une instruction if qui met le texte de l'hyperlien en majuscules si cet élément a la classe active.
console.groupCollapsed('6')
let menuLiElements = $('nav').find('li')
console.log('todo (6):menuLiElements:', ????)
console.log('todo (6):linkElements:', ????)
console.log("todo (6):linkElements.hasClass('active'):", ????)

console.groupEnd()

// todo (7) : Logguez le tagName de cet hyperlien (méthode .prop("tagName")). De la même façon, logguez son attribut href (méthode .attr("href")).
// Modifiez cet attribut en "#" et vérifiez les modifications en inspectant le DOM.
console.groupCollapsed('7')
console.log('todo (7):linkElements.prop("tagName"):', ????)
console.log('todo (7):linkElements.attr("href"):', ????)
console.groupEnd()

// todo (8) : Partez de la variable linkElements précédente. Remontez à son parent, le li, à l'aide de (.parent()).
// Logguez dans la console sa propriété "id" (.prop("id")). Qu'observez-vous pour l'id ?. Finalement, changez cet id en "mi2".
console.groupCollapsed('8')
console.log('todo (8):linkElements.parent().prop("id"):', ????)
console.log('todo (8):linkElements.parent().prop("id"):', ????)
console.groupEnd()

// todo (9) : Sélectionnez l'élément input dont le type est range et placez le dans une variable nommée ageInput et logguez la varaible.
// Partez de cette variable, et changez son attribut max à 99 (.attr()) et sa valeur (value) à 55 (.val()). Vérifiez les modifications en inspectant le DOM.
console.groupCollapsed('9')
let ageInput = $("input[type='range']")
console.log('todo (9):ageInput:', ????)

console.groupEnd()

// todo (10) : Sélectionnez le input dont le "name" est "jquery" et placez le dans une variable nommée jQueryInput.
// Affichez la console la valeur de la propriété checked (.prop("checked")). Puis inversez cet état SANS TENIR COMPTE DE LA VALEIUR INITIALE. Vérifiez dans la page.
console.groupCollapsed('10')
console.log('todo (10):jQueryInput.prop("checked")', ????)
jQueryInput.prop('checked', ????)
console.groupEnd()

// todo (11) : Sélectionnez les images contenues dans l'élément #image (un div) et placez cette sélection dans une variable nommée smileImages.
// Logguez la variable smileImages. Affichez dans la console la largeur de la première des deux images (méthode .width()) puis sa hauteur (méthode .height()).
// Toujours pour la première des deux images, affichez dans la console ses positions top et left rapport à leur parent (.position()), puis par rapport au document entier (.offset())
console.groupCollapsed('11')
console.log('todo (11):smileImages:', ????)
console.log('todo (11):smileImages.eq(0).width()', ????)
console.log('todo (11):smileImages.eq(0).height()', ????)
console.log('todo (11):smileImages.eq(0).position()', ????)
console.log('todo (11):smileImages.eq(0).offset()', ????)
console.groupEnd()

// todo (12) : Modifiez la largeur de la première des deux images en 250 (.width(250)). Il n'y a pas d'unité ici, ce sont des pixels toujours.
// Modifiez la position de la seconde image par rapport au document ( .offset({top:25, left:100}).
console.groupCollapsed('12')

console.groupEnd()
