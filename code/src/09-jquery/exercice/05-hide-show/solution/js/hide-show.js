'use strict'

console.log('Salut jQuery !')

// todo (1) : Mettre tous les descendants div du div#container dans une variable jQuery nommée "divs", et logguez cette variable
const divs = $('#container').find('div')
console.log('todo (1):divs:', divs)

// todo (2) : Pour le 1er des divs (utilisez la méthode .eq()), cachez le immédiatement (.hide()). Vérifier dans l'inspecteur que l'élément est en display:none;
divs.eq(0)
    .hide()

// todo (3) : Pour le 2ième des divs (méthode .eq()), en utilisant le chaining : cachez le "lentenent" (.hide("slow")), puis montrez le "rapidement" (.show("fast"))
// hide() / show()
divs.eq(1)
    .hide('slow')
    .show('fast')

// todo (4) : Pour le 3ième des divs, cachez le en 5000ms avec effet de fading (cad par son opacité, .fadeOut(5000)). Puis suspendez les animations ultérieures pendant 4000 ms. Puis faites réapparaitre l'élément en 4000ms (.fadeIn(4000))
divs.eq(2)
    .fadeOut(5000)
    .delay(4000)
    .fadeIn(3000)

// todo (5) : Pour le le 4ième des div, amenez le jusqu'à une opacité finale de 0.3 en 5000ms avec un effet de "swing" (.fadeTo(5000, 0.3, "swing", function(){...})). Quand l'effet est terminé, attribuez la class CSS "active" à l'élément. Pour cela, utilisez le dernier argument qui est une fonction "callback". Cette fonction sera appelée à la fin de l'animation. Dans le code de la fonction, mettez la classe CSS à l'élément. Finalement, enchainé à la méthode fadeOut(), faites revenir cet élément à une opacité de 0.6 en 4000ms .fadeTo(4000, 0.6)
divs.eq(3)
    .fadeTo(4000, 0.5, 'swing', function () {
        console.log('Fin du premier fadeTo()')
        $(this).addClass('active')
    })
    .fadeTo(4000, 0.6)

// todo (6) Fermez le 5ième div en 4000ms (.slideUp()) et quand la fermeture est complète, rouvrez-le en 4000ms (.slideDown())
divs.eq(4)
    .slideUp(4000)
    .slideDown(4000)
