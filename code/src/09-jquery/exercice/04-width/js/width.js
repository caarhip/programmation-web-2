'use strict'

/**
 * Compléter le code manquant pour obtenir le même résultat que les maquettes.
 *
 * Pour chacun des <div> contenu dans le <div id="cibles"> ajouter une balise <span class="etiquette"> contenant la largueur en px de la balise <div> apres application du CSS.
 */

$(document).ready(function () {
    $('#cibles').children('div').click(function () {
        const largueur = $(this).width().toFixed(2)

        $(this).html('<span class = "etiquette">' + largueur + 'px </span>')
    })
})
