'use strict'

$(document).ready(function () {
    $('#cibles').children('div').click(function () {
        const largeur = $(this).width().toFixed(2)

        $(this).html('<span class="etiquette">' + largeur + '</span>')
    })
})
