'use strict'

/**
 * Compléter le code manquant pour obtenir le même résultat que la maquette
 */

console.log('Salut jQuery !')

// (1) Afficher dans la console le innerHTML du div d'id "premier"
console.log($('#premier').html())
// (2) Afficher dans la console le nombre d'éléments de classe CSS "info"
console.log($('.info').length)
// (3) Afficher dans la console le nombre total d'images de la page
console.log($('img').length)
// (4) Afficher dans la console le nombre d'images contenues dans le div d'id "premier"
console.log($('#premier img').length)
// (5) Afficher dans la console le nombre d'images dont la source a un path qui contient le mot "gallery"
console.log($("img[src*='gallery']").length)
// (6) Déclarez une variable jQ nommée firstDiv qui contient l'élément d'id "premier"
const firstDiv = $('#premier')
// (7) En utilisant cette variable cette fois, afficher dans la console le nombre d'images de cet élément #premier et dont la source contient le sous-chaine "gallery"
console.log($(firstDiv).find("img[src*='gallery']").length)
// (8) Déclarez une variable firstDivById qui contient l'élément DOM d'id "premier" (le même que précédemment), utilisez document.getElementById
const firstDivById = document.getElementById('premier')
// (9) "jQuerisez" cette variable firstDivById (élément du DOM), c'est à dire passez la dans la fonction $(), et affichez le nombre d'enfants directs de type p qu'il contient
// console.log($(firstDiv).)
