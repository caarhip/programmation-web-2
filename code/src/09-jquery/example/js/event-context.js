$('div').on('click', function (event) {
    console.group('Exécution de l\'évenement click sur la balise DIV')
    console.log('event.target', event.target)
    console.log('event.currentTarget', event.currentTarget)
    console.log('this', this)
    console.groupEnd()
})

$('p').on('click', function (event) {
    console.group('Exécution de l\'évenement click sur la balise P')
    console.log('event.target', event.target)
    console.log('event.currentTarget', event.currentTarget)
    console.log('this', this)
    console.groupEnd()
})
