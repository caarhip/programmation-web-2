'use strict'

// eslint-disable-next-line no-unused-vars
function readForm () {
    const lastName = document.getElementById('lastName')
    const firstName = document.getElementById('firstName')

    let result = '<p>' + lastName.value + '</p>'
    result += '<p>' + firstName.value + '</p>'

    const output = document.getElementById('output')
    output.innerHTML = result
}
