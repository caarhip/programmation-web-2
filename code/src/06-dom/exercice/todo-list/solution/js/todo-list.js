'use strict'

// eslint-disable-next-line no-unused-vars
function addNewLiElement () {
    const textBox = document.getElementById('textBox')
    var textContent = textBox.value.trim()

    textBox.value = ''

    if (textContent.length > 0) {
        const newLiElement = document.createElement('LI')
        newLiElement.appendChild(document.createTextNode(textContent))
        document.getElementById('liste').appendChild(newLiElement)
    }
}

// eslint-disable-next-line no-unused-vars
function deleteLastLiElement () {
    var conteneur = document.getElementById('liste')

    var lastLiElement = conteneur.lastElementChild

    if (lastLiElement != null) {
        conteneur.removeChild(lastLiElement)
    }
}
