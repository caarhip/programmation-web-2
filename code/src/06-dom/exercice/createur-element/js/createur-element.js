'use strict'

const typeOfContent = document.getElementById('elementType')
const valueOfContent = document.getElementById('elementContent')
const output = document.getElementById('output')

// eslint-disable-next-line no-unused-vars
function createWithInnerHTML () {
    output.innerHTML += '<' + typeOfContent.value + ' class="inner-html">' + valueOfContent.value + '</' + typeOfContent.value + '>'
}

// eslint-disable-next-line no-unused-vars
function createWithCreateElement () {
    const content = document.createElement(typeOfContent.value)
    content.classList.add('create-element')
    const text = document.createTextNode(valueOfContent.value)
    content.appendChild(text)
    output.appendChild(content)
}
