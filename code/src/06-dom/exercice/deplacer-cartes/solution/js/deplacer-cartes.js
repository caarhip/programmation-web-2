'use strict'

const cartesContainer = document.getElementById('cartes')

// eslint-disable-next-line no-unused-vars
function tournerDroite () {
    cartesContainer.insertBefore(cartesContainer.lastElementChild, cartesContainer.firstElementChild)

    // cartesContainer.insertAdjacentElement('afterbegin', cartesContainer.lastElementChild)
}

// eslint-disable-next-line no-unused-vars
function tournerGauche () {
    // Ci dessous pour "remplacer" le insertAfter qui n'existe pas
    cartesContainer.insertBefore(cartesContainer.firstElementChild, cartesContainer.lastElementChild.nextSibling)

    // Autre approche ici:
    // cartesContainer.appendChild(cartesContainer.firstElementChild);
    // cartesContainer.insertAdjacentElement('beforeend', cartesContainer.firstElementChild)
}
