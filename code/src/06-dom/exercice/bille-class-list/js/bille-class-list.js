'use strict'

const billes = document.getElementsByClassName('bille')

billes[0].classList.add('arrondi')

billes[1].classList.add('orange', 'taille-x')

billes[2].classList.add('taille-xx', 'incline')

billes[3].classList.add('mauve', 'arrondi', 'decale')

billes[4].classList.add('taille-xxx', 'arrondi', 'mauve')
