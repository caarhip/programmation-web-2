'use strict'

window.addEventListener('DOMContentLoaded', function loaded (event) {
    window.removeEventListener('DOMContentLoaded', loaded, false)

    document.getElementById('taskInput').focus()
    document.getElementById('taskInput').select()

    const inputTask = document.getElementById('taskInput')
    const boutonAjout = document.getElementById('addButton')
    const deleteButton = document.getElementById('deleteButton')
    const liste = document.getElementById('taskList')

    console.log(inputTask)

    inputTask.addEventListener('input', function () {
        boutonAjout.disabled = (this.value === '')
    })

    boutonAjout.addEventListener('click', function (event) {
        addElementToList()
    })

    inputTask.addEventListener('keyup', function (event) {
        if (event.keyCode === 13) {
            event.preventDefault()
            addElementToList()
            inputTask.value = ''
        }
    })

    deleteButton.addEventListener('click', function (event) {
        const listeTaches = document.getElementsByClassName('list-element')

        Array.from(listeTaches).forEach(function (tache) {
            const checkInput = tache.children[0]

            if (checkInput.checked) {
                tache.parentNode.removeChild(tache)
            }
        })
    })

    function addElementToList () {
        const tacheSoumise = inputTask.value
        const listeElement = document.createElement('li')
        listeElement.classList.add('list-element')
        listeElement.innerHTML = '<input type="checkbox">' + '<span>' + tacheSoumise + '</span>'
        liste.appendChild(listeElement)

        const listeTaches = document.getElementsByClassName('list-element')

        Array.from(listeTaches).forEach(function (tache) {
            const checkInput = tache.children[0]
            const texteSpan = tache.children[1]

            checkInput.addEventListener('click', function (event) {
                deleteButton.disabled = !checkInput.checked
            })

            texteSpan.addEventListener('click', function (event) {
                texteSpan.classList.add('strike-task')
            })
        })
    }
}, false)
