'use strict'

function createItem (item) {
    return '<li>' + item + '</li>'
}

// eslint-disable-next-line no-unused-vars
function createList (items) {
    let result = '<ul>'

    for (let counter = 0; counter < items.length; counter++) {
        result += createItem(items[counter])
    }

    result += '</ul>'
    return result
}
