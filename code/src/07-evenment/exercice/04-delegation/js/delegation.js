'use strict'

window.addEventListener('DOMContentLoaded', function loaded (event) {
    window.removeEventListener('DOMContentLoaded', loaded, false)

    const listContainer = document.getElementById('listContainer')

    for (let counter = 0; counter < 10000; counter++) {
        const liElement = document.createElement('li')
        liElement.setAttribute('id', 'product_no_' + counter)
        liElement.appendChild(document.createTextNode('Produit numéro: ' + counter))
        listContainer.appendChild(liElement)
    }
}, false)
