'use strict'

const JOURS_FR = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
const JOURS_EN = ['Monday', 'Tuesday', 'Wednesday', 'Thurday', 'Friday', 'Saturday', 'Sunday']

window.addEventListener('DOMContentLoaded', function () {
    const language = document.getElementById('language')
    const dayNumber = document.getElementById('dayNumber')

    dayNumber.addEventListener('input', function () {
        updateDisplay(language, dayNumber)
    })

    language.addEventListener('change', function () {
        updateDisplay(language, dayNumber)
    })

    updateDisplay(language, dayNumber)
})

function updateDisplay (language, dayNumber) {
    const JOURS = language.checked ? JOURS_EN : JOURS_FR

    document.querySelector('label[for=dayNumber]').innerHTML = JOURS[dayNumber.value]
}
