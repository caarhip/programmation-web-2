'use strict'

const numberInput = document.getElementById('numberInput')

numberInput.addEventListener('input', updateDisplay)
const numberDiv = document.getElementById('numberDiv')

function updateDisplay () {
    const inputValue = numberInput.value

    if ((numberInput.value < 10) && ('0123456789'.indexOf(numberInput.value) > -1)) {
        numberDiv.innerHTML = '<img src="image/' + inputValue + '.jpg" alt="image du chiffre ' + inputValue + '">'
    }
}
