'use strict'

const numberInput = document.getElementById('numberInput')
numberInput.addEventListener('input', changePicture)

const numberDiv = document.getElementById('numberDiv')

function changePicture () {
    const inputValue = numberInput.value

    numberDiv.innerHTML = '<img src="image/' + inputValue + '.jpg" alt="image du chiffre ' + inputValue + '">'
}
