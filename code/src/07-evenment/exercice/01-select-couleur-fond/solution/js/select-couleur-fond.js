'use strict'

const colorSelect = document.getElementById('colorSelect')
colorSelect.addEventListener('change', function (event) {
    document.body.style.backgroundColor = event.target.value
})
