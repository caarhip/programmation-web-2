
document.getElementById('testForm').addEventListener('submit', handleFormSubmit)

document.getElementById('submitButton').addEventListener('click', handleButtonClick)

function handleFormSubmit (event) {
    console.log('form', event)

    event.preventDefault()
}

function handleButtonClick (event) {
    console.log('button', event)
}
