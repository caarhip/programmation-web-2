
document.getElementById('containerElement').addEventListener('click', containerClickHandler)

document.getElementById('buttonElement').addEventListener('click', buttonClickHandler)

function containerClickHandler (event) {
    console.log('containerElement', event.target, event.currentTarget)
}

function buttonClickHandler (event) {
    console.log('buttonElement', event.target, event.currentTarget)
}
