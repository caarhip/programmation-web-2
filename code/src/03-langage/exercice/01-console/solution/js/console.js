'use strict'

const temperature = 21.3
const fruits = ['pomme', 'poire', 'orange']

// Faire le ménage pour commencer
console.clear()

// Texte littéral (constante chaine littérale)
console.log('Salut.')

console.log(temperature)

console.log(fruits)

// Texte puis variable
console.log('La température est ', temperature)
console.log('La collection de fruits est', fruits)

// Pas d'interpréteur HTML dans la console
console.log("<h1>Pas d'interpréteur HTML</h1>")

// La console montre les types en couleur
console.log(18, 'dis-huit', true, null, undefined)

// Pour grouper dans la console
console.group('Ceci est un groupe à part')
console.log('Contenu du groupe')
console.groupEnd()

// Pour grouper dans la console (groupe replié par défaut)
console.groupCollapsed('Ceci est un second groupe à part (fermé par défaut)')
console.log('Contenu du second groupe')
console.groupEnd()
